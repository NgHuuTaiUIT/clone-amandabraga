import React from "react";
import CustomCursor from "../components/common/custom-cursor";

export default {
  title: "CustomCursor",
  component: CustomCursor
};
export const CustomCursorDefault = () => {
  return (
    <div style={{ backgroundColor: "red", width: "100vw", height: "100vh" }}>
      <CustomCursor />
    </div>
  );
};
